import BaseHTTPServer
import cgi
import random
import sys
import os
import multiprocessing.dummy as mp
import subprocess
from threading import Thread
import shutil
import socket
import requests
import json

response = []
server_url = 'http://' + socket.gethostbyname(socket.gethostname()) + ':8000/'


def send_file(video_input, set_format):
    files = {'file': open(video_input, 'rb')}
    values = {'format': set_format}
    host = 'http://88.135.36.66'
    url =host + '/api/mrestate/v1/upload_video'
    print 'backup file sending ...'
    r = requests.post(url, files=files, data=values)
    get_json = json.loads(r.text)
    response.append(host + str(get_json['url']))
    print host + str(get_json['url'])


def convert_video(video_input, video_output, format):
    cmds = ['ffmpeg', '-i', video_input, video_output]
    p = subprocess.Popen(cmds)
    p.wait()
    response.append(server_url + video_output)
    print server_url + video_output
    send_file(video_output, format)
    print 'file uploaded'


def send_backup_video(video_input):
    cmds = ['ffmpeg', '-i', video_input, video_output]
    p = subprocess.Popen(cmds)
    p.wait()
    print server_url + video_output
    response.append(server_url + video_output)


class SimpleRequestHandler(BaseHTTPServer.BaseHTTPRequestHandler):

    def _set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):
        path = '.' + self.path
        encodedFilePath = path.replace('./', '')

        with open(encodedFilePath, 'rb') as videoFile:
            self.send_response(200)
            self.send_header("Content-Type", 'application/octet-stream')
            self.send_header("Content-Disposition", 'attachment; filename="{}"'.format(open(encodedFilePath)))
            fs = os.fstat(videoFile.fileno())
            self.send_header("Content-Length", str(fs.st_size))
            self.end_headers()
            shutil.copyfileobj(videoFile, self.wfile)

    def do_POST(self):

        form = cgi.FieldStorage(
            fp=self.rfile,
            headers=self.headers,
            environ={'REQUEST_METHOD': 'POST',
                     'CONTENT_TYPE': self.headers['Content-Type'],
                     })
        format = form['format'].value
        get_formats = format.split(',')

        filename = form['file'].filename
        data = form['file'].file.read()
        open('get_videos/' + filename, "wb").write(data)
        name = str(random.randint(11111, 99999))
        threads = []

        for frm in get_formats:
            process = Thread(target=convert_video,
                             args=['get_videos/' + filename, '1/20012/' + name + '.' + frm, frm])
            process.start()

            threads.append(process)

        for process in threads:
            process.join()

        self._set_headers()
        self.wfile.write(
            response
        )


def run(server_class=BaseHTTPServer.HTTPServer, handler_class=SimpleRequestHandler):
    server_address = ('', 8000)
    httpd = server_class(server_address, handler_class)
    httpd.serve_forever()


run()