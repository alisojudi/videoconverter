import requests
import json


def send_file(video_input, set_format):
    files = {'file': open(video_input, 'rb')}
    values = {'format': set_format}
    host = 'http://88.135.36.66'
    url =host + '/api/mrestate/v1/upload_video'
    r = requests.post(url, files=files, data=values)
    get_json = json.loads(r.text)
    return host + str(get_json['url'])


send_file()